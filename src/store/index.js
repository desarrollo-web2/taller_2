import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    /* Used for determine if FormDialog will be used to Create or Edit a product */
    editedIndex: -1,
    /* Used for make dialogs accesible from other components */
    dialog: false,
    /* Default Initial Product Data */
    products: [
      {
        name: "PlantUI",
        description: "eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus viverra vitae congue eu consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas",
        price: 56000,
        quantity: 10,
        imageURL: 'https://cdn.trendhunterstatic.com/thumbs/plantui-6.jpeg'
      },
      {
        name: "Smart Pot - DAV-01",
        description: "eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus viverra vitae congue eu consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas",
        price: 29990,
        quantity: 50,
        imageURL: 'https://cdn.trendhunterstatic.com/thumbs/plantui-6.jpeg'
      },
      {
        name: "Plant Robot",
        description: "eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus viverra vitae congue eu consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas",
        price: 1990,
        quantity: 2,
        imageURL: 'https://cdn.trendhunterstatic.com/thumbs/plantui-6.jpeg'
      },
      {
        name: "iGarden",
        description: "eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus viverra vitae congue eu consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas",
        price: 39990,
        quantity: 1,
        imageURL: 'https://cdn.trendhunterstatic.com/thumbs/plantui-6.jpeg'
      },
    ],
    cart: [],
    history: []
  },
  mutations: {
    addProduct(state, payload) {
      state.products.push(payload);
    },
    setEditedIndex(state, payload) {
      state.editedIndex = payload;
    },
    /* This makes dialog accesible from Parent */
    setFormDialogVisible(state, payload) {
      state.dialog = payload;
    },
    /* Recieves new product data and assigns it to the product in products[] */
    editProduct(state, payload) {
      Object.assign(state.products[state.editedIndex], payload);
    },
    /* Recieves a new product, gets its index and splice it from products[] */
    deleteProduct(state, payload) {
      const index = state.products.indexOf(payload)
      state.products.splice(index, 1);
    },
    /* Recievevs a product from Cart, gets index and delete in cart[] */
    deleteProductFromCart(state, payload) {
      const index = state.products.indexOf(payload)
      state.cart.splice(index, 1);
    },
    /* Recieves a product, get its index and then update quantity */
    buyProducts(state) {
      state.cart.forEach(element => {
        state.history.push(element)
      });
      state.cart = [];
    },
    addProductToCart(state, payload) {
      state.cart.push(payload);
    },
    resetCart(state) {
      state.cart = [];
    }
  },
  actions: {},
  modules: {},
  getters: {
    getAllProducts: state => state.products,
    getNumberOfCartProducts: state => {
      var counter = 0;
      state.cart.forEach(element => {
        counter += Number(element.quantity);
      });
      return counter;
    },
    getTotalMoney: state => {
      var counter = 0
      state.history.forEach(element => {
        counter = (counter + (Number(element.price) * Number(element.quantity)));
      });
      return counter;
    },
    getNumberOfSales: state => {
      var counter = 0
      state.history.forEach(element => {
        counter += Number(element.quantity);
      });
      return counter;
    }
  }
});
