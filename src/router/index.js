import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Cart from "../views/Cart.vue";
import Store from "../views/Store.vue";
import Shop from "../views/Shop.vue";
import History from "../views/History.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/almacen",
    name: "Store",
    component: Store
  },
  {
    path: "/carrito",
    name: "Cart",
    component: Cart
  },
  {
    path: "/tienda",
    name: "Shop",
    component: Shop
  },
  {
    path: "/historial",
    name: "History",
    component: History
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
